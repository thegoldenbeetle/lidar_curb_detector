"""Program for curb detection by lidar cloud points."""
import argparse
from datetime import datetime

import numpy as np
from open3d._ml3d.utils.config import Config as Open3dConfig

from src.curb_detector import CurbDetector
from src.data_processing import Dataset, TorontoCategories
from src.general_detector import GeneralDetector


def parse_args() -> argparse.Namespace:
    """Command line argument parsing.

    Returns
    -------
    argparse.Namespace
        Namespace with arguments and their values.
    """
    parser = argparse.ArgumentParser(description="Curb detection program")
    parser.add_argument(
        "--input",
        default="./data/Toronto_3D/L004.ply",
        type=str,
        required=True,
        help="Input filepath from Toronto dataset",
    )
    parser.add_argument(
        "--cfg",
        default="./config.yml",
        type=str,
        required=False,
        help="Config filepath",
    )
    parser.add_argument(
        "--output",
        default=None,
        type=str,
        required=False,
        help="Output filepath with curb points",
    )
    return parser.parse_args()


def parse_cfg(cfg_file_path: str) -> Open3dConfig:
    """Config parsing.

    Parameters
    ----------
    cfg : str
        Path to config with parameters.

    Returns
    -------
    open3d._ml3d.utils.config
        Parsed config object with parameters values.
    """
    return Open3dConfig.load_from_file(cfg_file_path)


def get_output_filename() -> str:
    """Generate default output filename.

    Returns
    -------
    str
        Str in format 'borders_{current_date_time}.las'
    """
    date_time_str = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return f"borders_{date_time_str}.las"


if __name__ == "__main__":

    args = parse_args()
    cfg = parse_cfg(args.cfg)

    data = Dataset.create_from_file(args.input)
    offsetted_data = data.get_offsetted(cfg)

    data_to_count = offsetted_data
    data_mask = np.full((len(data_to_count)), True)
    if cfg.dataset.enable_slice:
        data_to_count, data_mask = offsetted_data.choose_slice(cfg)

    print("Road detection...")
    general_detector = GeneralDetector(cfg)
    point_labels = general_detector.run_inference(data_to_count)
    road_data = data[data_mask][point_labels == TorontoCategories.GROUND.value]

    print("Сurb detection...")
    curb_detector = CurbDetector(cfg)
    curb_labels = curb_detector.run_inference(road_data)
    curb_data = road_data[curb_labels]

    output_file = (
        args.output if args.output is not None else get_output_filename()
    )
    print(f"Save result to {output_file}...")
    curb_data.points_to_las(output_file)
