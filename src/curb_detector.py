"""Module for curb points detections."""
from collections import Counter
from dataclasses import dataclass
from typing import Tuple

import numpy as np
from open3d._ml3d.utils.config import Config as Open3dConfig
from scipy import stats
from scipy.ndimage import gaussian_filter
from sklearn.cluster import DBSCAN
from tqdm import tqdm

from .data_processing import Dataset


@dataclass
class Voxel:
    """2d voxel data."""

    points: np.ndarray
    indexies: np.ndarray
    size: float


@np.vectorize
def elevation_difference(voxel: Voxel, empty_value: float = 0.0) -> float:
    """Count difference in height in voxel.

    Parameters
    ----------
    voxel : Voxel
        Input voxel
    empty_value: float
        Value if voxel is None. Default is 0.0

    Returns
    -------
    float
        Height difference
    """
    if voxel is not None:
        return voxel.points[:, 2].max() - voxel.points[:, 2].min()
    return empty_value


@np.vectorize
def elevation_map(voxel: Voxel, empty_value: float = 0.0) -> float:
    """Count mean value for height in voxel.

    Parameters
    ----------
    voxel : Voxel
        Input voxel
    empty_value: float
        Value if voxel is None. Default is 0.0.

    Returns
    -------
    float
        Mean value
    """
    if voxel is not None:
        return voxel.points[:, 2].mean()
    return empty_value


def conv_sobel(a: np.ndarray, axis: int = 0) -> np.ndarray:
    """Count sobel operator.

    Parameters
    ----------
    a : np.array
        Input matrix
    axis: int
        Axis for determine kernel. Default is 0.

    Returns
    -------
    np.ndarray
        Matrix after convolution
    """
    if axis == 0:
        kernel = np.array(
            [
                [-1, 0, 1],
                [-2, 0, 2],
                [-1, 0, 1],
            ]
        )
    else:
        kernel = np.array(
            [
                [1, 2, 1],
                [0, 0, 0],
                [-1, -2, -1],
            ]
        )
    x_size, y_size = a.shape
    new_image = np.zeros((x_size, y_size))
    for x in range(1, x_size - 1):
        for y in range(1, y_size - 1):
            p = a[x - 1:x + 2, y - 1:y + 2] * kernel
            if axis == 0:
                for i in range(3):
                    if np.any(np.isnan(p[i, :])):
                        p[i, :] = 0
            else:
                for i in range(3):
                    if np.any(np.isnan(p[:, i])):
                        p[:, i] = 0
            new_image[x, y] = p.sum()
    return new_image


class CurbDetector:
    """Detector for curbs."""

    def __init__(self, cfg: Open3dConfig):
        self.voxel_size: float = cfg.curb_detector["voxel_size"]
        self.min_height: float = cfg.curb_detector["min_height"]
        self.max_height: float = cfg.curb_detector["max_height"]
        self.min_gradient: float = cfg.curb_detector["min_gradient"]
        self.max_gradient: float = cfg.curb_detector["max_gradient"]
        self.use_gaussian: bool = cfg.curb_detector["use_gaussian"]
        self.enable_cluster: bool = cfg.curb_detector["enable_cluster"]
        self.cluster_eps: float = cfg.curb_detector["cluster_eps"]
        self.cluster_min_count: int = cfg.curb_detector["cluster_min_count"]

    def run_inference(self, data: Dataset) -> np.ndarray:
        """Find curbs.

        Parameters
        ----------
        data : Dataset
            Input Dataset

        Returns
        -------
        np.ndarray
            Mask with curbs points.
        """
        voxel_map = self.create_voxel_map(
            data.point,
            voxel_size=self.voxel_size,
        )
        ed_filter = self.elevation_difference_filter(
            voxel_map,
            min_height=self.min_height,
            max_height=self.max_height,
        )
        em_x_filter, em_y_filter = self.elevation_gradient_filter(
            voxel_map,
            min_gradient=self.min_gradient,
            max_gradient=self.max_gradient,
            use_gaussian=self.use_gaussian,
        )

        end_filter = (em_x_filter | em_y_filter) & ed_filter
        indexies = []
        for voxel in voxel_map[end_filter]:
            indexies.extend(voxel.indexies)
        mask = np.zeros(len(data.point), dtype=bool)
        mask[indexies] = True

        if self.enable_cluster:
            cluster_mask = self.cluster_filter(
                data.point[mask],
                eps=self.cluster_eps,
                min_count=self.cluster_min_count,
            )
            mask[indexies][~cluster_mask] = False
        return mask

    @staticmethod
    def elevation_gradient_filter(
        voxel_map: np.ndarray,
        min_gradient: float = 0.1,
        max_gradient: float = 0.25,
        use_gaussian: bool = True,
    ) -> Tuple[np.ndarray, np.ndarray]:
        """Filter 2d voxel grid by elevation gradient.

        Parameters
        ----------
        voxel_map : np.ndarray
            2d voxel grid.
        min_gradient : float
            Minimal gradient. Default is 0.1.
        max_gradient : float
            Minimal gradient. Default is 0.25.
        use_gausian : bool
            Using gausian filter on gradient map. Defautl is True.

        Returns
        -------
        numpy.ndarray
            Elevation gradient by X axis.
        numpy.ndarray
            Elevation gradient by Y axis.
        """
        i = elevation_map(voxel_map)
        if use_gaussian:
            i = gaussian_filter(i, sigma=1)
        i_interp = i.copy()
        i_interp[voxel_map == None] = np.nan

        g_x = np.abs(conv_sobel(i_interp, axis=0))
        em_x_filter = (g_x > min_gradient) & (g_x < max_gradient)

        g_y = np.abs(conv_sobel(i_interp, axis=1))
        em_y_filter = (g_y > min_gradient) & (g_y < max_gradient)

        return em_x_filter, em_y_filter

    @staticmethod
    def cluster_filter(
        points: np.ndarray,
        eps: float = 0.25,
        min_count: int = 50,
    ) -> np.ndarray:
        """Cluster filtration.

        Using DBSCAN for filtration outliners.

        Parameters
        ----------
        points : np.ndarray
            Input points.
        eps : float
            DBSCAN eps parameter.
            Default is 0.25.
        min_count : int
            Minimal points counts for cluster.
            Default is 50.

        Returns
        -------
        np.ndarray
            Filtered points.
        """
        clustering = DBSCAN(eps=eps, min_samples=2).fit(points)
        labels = clustering.labels_
        mask = np.zeros(points.shape[0], dtype=bool)
        for c, n in Counter(labels).items():
            if n >= min_count:
                mask &= labels == c
        return mask

    @staticmethod
    def elevation_difference_filter(
        voxel_map: np.ndarray,
        min_height: float = 0.1,
        max_height: float = 0.25,
    ) -> np.ndarray:
        """Filter voxels by height.

        Parameters
        ----------
        voxel_map : numpya.ndarray
            2d voxel grid.
        min_height : float
            Minimal height. Default is 0.1.
        max_height : float
            Maximal height. Default is 0.25.

        Returns
        -------
        numpy.ndarray
            Bool mask for voxel grid
        """
        height = elevation_difference(voxel_map)
        return (height > min_height) & (height < max_height)

    @staticmethod
    def create_voxel_map(
        points: np.ndarray, voxel_size: float = 0.25
    ) -> np.ndarray:
        """Crate 2d voxel grid for pointcloud.

        Parameters
        ----------
        points : numpy.ndarray
            input points.
        voxel_size : float
            2d voxel size.

        Returns
        -------
        numpy.ndarray of Optionl[Voxel]
            2d voxel grid.
        """
        xmin, ymin, _ = points.min(axis=0)
        xmax, ymax, _ = points.max(axis=0)

        binx = np.linspace(
            xmin - 1e-8, xmax + 1e-8, int((xmax - xmin) / voxel_size)
        )
        biny = np.linspace(
            ymin - 1e-8, ymax + 1e-8, int((ymax - ymin) / voxel_size)
        )
        s = stats.binned_statistic_2d(
            points[:, 0],
            points[:, 1],
            None,
            "count",
            bins=[binx, biny],
            expand_binnumbers=True,
        )

        voxel_map = np.empty(
            (
                len(binx) - 1,
                len(biny) - 1,
            ),
            dtype=Voxel,
        )
        voxel_iterator = np.zeros(
            (
                len(binx) - 1,
                len(biny) - 1,
            ),
            dtype=int,
        )
        for index, (i, j) in tqdm(
            enumerate(s.binnumber.T), total=s.binnumber.shape[1]
        ):
            i = i - 1
            j = j - 1
            if voxel_map[i, j] is None:
                voxel_map[i, j] = Voxel(
                    np.empty((int(s.statistic[i, j]), 3), dtype=points.dtype),
                    np.empty(int(s.statistic[i, j]), dtype=int),
                    voxel_size,
                )
            voxel_map[i, j].points[voxel_iterator[i, j]] = points[index]
            voxel_map[i, j].indexies[voxel_iterator[i, j]] = index
            voxel_iterator[i, j] += 1
        return voxel_map
