"""Module for specific points detections.

This detections can be usefull for further curb detection.

"""
from typing import Tuple

import numpy as np
import open3d.ml.torch as ml3d
import torch
from open3d._ml3d.utils.config import Config as Open3dConfig

from .data_processing import Dataset


class GeneralDetector:
    """Detector for Toronto categories."""

    def __init__(self, cfg: Open3dConfig):
        self.cfg = cfg
        model = ml3d.models.RandLANet(**self.cfg.detector_model)
        self.pipeline = CustomSemanticSegmentation(
            model, main_log_dir="/tmp", **self.cfg.detector_inference
        )
        self.pipeline.load_ckpt(self.cfg.detector_model["ckpt_path"])
        self.cpu_device = torch.device("cpu")

    def data_preprocess(self, data: Dataset) -> Tuple[dict, np.ndarray]:
        """Preprocess data for further processing by model.

        Cut off points by z dim threshold.

        Parameters
        ----------
        data : Dataset
            Data for detection.

        Returns
        -------
        dict
            Model input and cutoff indices.
        numpy.ndarray
            Cutoff indices.
        """
        data = Dataset(
            data.point.astype(np.float32), data.feat.astype(np.float32)
        )

        p_z = data.point[:, 2]
        zmin, zmax = min(p_z), max(p_z)
        zmask = p_z <= (
            zmin + self.cfg.detector_inference["z_threshold"] * (zmax - zmin)
        )

        slice_point = data.point[zmask]
        slice_feat = data.feat[zmask]

        return {
            "point": torch.from_numpy(slice_point).to(self.cpu_device),
            "feat": torch.from_numpy(slice_feat).to(self.cpu_device),
        }, zmask

    def run_inference(self, data: Dataset) -> np.ndarray:
        """Model inference for Toronto categories detection.

        Parameters
        ----------
        data : Dataset
            Data for detections.

        Returns
        -------
        numpy.ndarray
            Categories labels.
        """
        preprocessed_data, mask = self.data_preprocess(data)

        results = self.pipeline.run_inference(preprocessed_data)

        labels = np.zeros(len(data.point))
        labels[mask] = results["predict_labels"] + 1

        return labels.astype(np.uint)


class CustomSemanticSegmentation(ml3d.pipelines.SemanticSegmentation):
    """Semantic segmentation pipeline class."""

    def run_inference(self, data: dict, with_metrics: bool = False) -> dict:
        """Model inference, overriden to not calculate metrics.

        Parameters
        ----------
        data : dict
            Data: data for inference.
        with_metrics : bool
            Flag to calculate metrics or not. Default is False.

        Returns
        -------
        dict
            Prediction results with labels and their scores.
        """
        if with_metrics is True:
            return super().run_inference(data)

        cfg = self.cfg
        model = self.model
        device = self.device
        model.to(device)
        model.device = device
        model.eval()

        batcher = self.get_batcher(device)
        infer_dataset = ml3d.datasets.InferenceDummySplit(data)
        self.dataset_split = infer_dataset
        infer_sampler = infer_dataset.sampler
        infer_split = ml3d.dataloaders.TorchDataloader(
            dataset=infer_dataset,
            preprocess=model.preprocess,
            transform=model.transform,
            sampler=infer_sampler,
            use_cache=False,
        )
        infer_loader = torch.utils.data.DataLoader(
            infer_split,
            batch_size=cfg.batch_size,
            sampler=ml3d.dataloaders.get_sampler(infer_sampler),
            collate_fn=batcher.collate_fn,
        )

        model.trans_point_sampler = infer_sampler.get_point_sampler()
        self.curr_cloud_id = -1
        self.test_probs = []
        self.test_labels = []
        self.ori_test_probs = []
        self.ori_test_labels = []

        with torch.no_grad():
            for _, inputs in enumerate(infer_loader):
                results = model(inputs["data"])
                self.update_tests(infer_sampler, inputs, results)

        return {
            "predict_labels": self.ori_test_labels.pop(),
            "predict_scores": self.ori_test_probs.pop(),
        }
