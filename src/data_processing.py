"""Module for operations with data from the Toronto dataset."""
import enum
from dataclasses import dataclass
from typing import Optional, Type, Tuple

import laspy
import numpy as np
import open3d as o3d
from open3d._ml3d.utils.config import Config as Open3dConfig


@dataclass
class Dataset:
    """Class for operating with data from Toronto dataset."""

    point: np.ndarray
    feat: np.ndarray

    @classmethod
    def create_from_file(cls: Type["Dataset"], input_file: str) -> "Dataset":
        """Read Toronto ply file.

        Parameters
        ----------
        input_file : str
            Filepath to .ply.

        Returns
        -------
        Dataset
            Dataset object with points positions and colors.
        """
        data = o3d.t.io.read_point_cloud(input_file).point
        return cls(data["positions"].numpy(), data["colors"].numpy())

    def points_to_las(
        self,
        output_file: str,
        labels: Optional[np.ndarray] = None,
    ):
        """Save points positions and labels to las file.

        Parameters
        ----------
        output_file : str
            Output filepath.
        labels : str, optional
            Category ids for points.
        """
        xyz = self.point[:, 0:3]

        hdr = laspy.LasHeader(point_format=6, version="1.4")
        hdr.offset = np.floor(np.min(xyz, axis=0))
        hdr.scale = [1e-5, 1e-5, 1e-5]

        las = laspy.LasData(hdr)

        las.x = xyz[:, 0]
        las.y = xyz[:, 1]
        las.z = xyz[:, 2]

        if labels is not None:
            las.classification = labels

        las.write(output_file)

    @staticmethod
    def get_mask(p_t: np.ndarray, k_t: float, s_t: float) -> np.ndarray:
        """Get bool mask for slicing for t dim.

        Parameters
        ----------
        p_t : numpy.ndarrray
            All points positions for t dim.
        k_t : float
            Coefficient for starting point.
        s_t : float
            Interval length.

        Returns
        -------
        numpy.ndarray
            Mask for specified interval.
        """
        tmin, tmax = min(p_t), max(p_t)
        tmin_t = tmin + k_t * (tmax - tmin)
        tmin, tmax = tmin_t - s_t, tmin_t + s_t
        return (p_t > tmin) & (p_t < tmax)

    def choose_slice(self, cfg: Open3dConfig) -> Tuple["Dataset", np.ndarray]:
        """Get part from dataset by slice.

        Parameters
        ----------
        cfg : open3d._ml3d.utils.config
            Config with slice parameters.

        Returns
        -------
        Dataset
            Dataset object with points positions and colors.
        numpy.ndarray
            Mask for slice.
        """
        p_x = self.point[:, 0]
        p_y = self.point[:, 1]
        xmask = self.get_mask(p_x, *cfg.dataset["x_slice"])
        ymask = self.get_mask(p_y, *cfg.dataset["y_slice"])
        mask = xmask & ymask
        return self[mask], mask

    def get_offsetted(self, cfg: Open3dConfig) -> "Dataset":
        """Apply offset for data points positions.

        Parameters
        ----------
        cfg : open3d._ml3d.utils.config
            Config with offset parameters.

        Returns
        -------
        Dataset
            Dataset with offset position.
        """
        return Dataset(self.point - cfg.dataset["offset"], self.feat)

    def __getitem__(self, key: np.ndarray) -> "Dataset":
        return Dataset(self.point[key], self.feat[key])

    def __len__(self):
        return len(self.point)


class TorontoCategories(enum.Enum):
    """Toronto dataset categories names."""

    GROUND = 1
    ROAD_MARKINGS = 2
    NATURAL = 3
    BUILDING = 4
    UTILITY_LINE = 5
    POLE = 6
    CAR = 7
    FENCE = 8
