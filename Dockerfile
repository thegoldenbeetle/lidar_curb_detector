FROM nvidia/cuda:11.4.3-devel-ubuntu20.04

ENV DUSER points
ENV DPASSWORD points
ENV PATH="/usr/local/cuda/bin:/home/${DUSER}/.local/bin:${PATH}"
ENV LD_LIBRARY_PATH="/usr/local/cuda/lib64:${LD_LIBRARY_PATH}"

# Install apt requirements
RUN apt-get update -y \
    && DEBIAN_FRONTEND="noninteractive" apt-get install -y sudo wget python3 python3-pip libgl1 \
    && rm -rf /var/lib/apt/lists/*

RUN useradd -ms /bin/bash ${DUSER} \ 
    && echo "${DUSER}:${DPASSWORD}" | chpasswd
RUN adduser ${DUSER} sudo

WORKDIR /app
RUN chown ${DUSER}:${DUSER} /app

USER ${DUSER}

COPY --chown=${DUSER}:${DUSER} ./requirements.txt /app/requirements.txt

# Install pip requirements
#ARG PYTORCH_URL=https://github.com/intel-isl/open3d_downloads/releases/download/torch1.7.1/torch-1.7.1-cp38-cp38-linux_x86_64.whl
#ARG PYTORCH_WHL=torch-1.7.1-cp38-cp38-linux_x86_64.whl
ARG PYTORCH_URL=https://github.com/isl-org/open3d_downloads/releases/download/torch1.8.2/torch-1.8.2-cp38-cp38-linux_x86_64.whl
ARG PYTORCH_WHL=torch-1.8.2-cp38-cp38-linux_x86_64.whl
RUN python3 -m pip install -U pip
RUN python3 -m pip install \
        --no-cache-dir \
        --ignore-installed \
        -r requirements.txt \
    && wget \
        --quiet \
        --show-progress \
        --progress=bar:force:noscroll \
        --no-check-certificate \
        ${PYTORCH_URL} \
        -O ${PYTORCH_WHL} \
    && python3 -m pip install \
        --no-cache-dir \
        ${PYTORCH_WHL} \
    && rm ${PYTORCH_WHL}
    
